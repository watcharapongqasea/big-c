*** Settings ***
Library           SeleniumLibrary

*** Keywords ***
_Click And Wait Element
    [Arguments]    ${locator}
    Wait Until Element Is Visible    ${locator}    30s
    Click Element    ${locator}

_Input And Wait Element
    [Arguments]    ${locator}    ${text}
    Wait Until Element Is Visible    ${locator}    30s
    Input Text    ${locator}    ${text}

_Open And Maximize Browser
    [Arguments]    ${url}    ${browser}
    Open Browser    ${url}    ${browser}
    Maximize Browser Window

_Select From DropDown By Label
    [Arguments]    ${locator}    ${label}
    Wait Until Element Is Visible    ${locator}
    Select From List By Label    ${locator}    ${label}

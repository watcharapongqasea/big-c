*** Keywords ***
Search Product
    Capture Page Screenshot
    _Input And Wait Element    //input[@id="search-id"]    ${product}
    Sleep    3s
    Capture Page Screenshot
    _Click And Wait Element    (//li[@class="search-list"])[1]
    ${status}    Run Keyword And Return Status    Wait Until Element Is Visible    //button[@type="button"]//span[.="สินค้าหมด"]
    Run Keyword If    '${status}'=='False'    _Click And Wait Element    (//span[.="เพิ่มสินค้า"])[1]
    Capture Page Screenshot

Open BigC Website
    _Open And Maximize Browser    https://www.bigc.co.th/    gc
    Sleep    20s
    Run Keyword And Ignore Error    _Click And Wait Element    (//div[@class="om-global-close-button om-popup-close"])[1]

Select Product Hightlight
    [Arguments]    ${category}
    Capture Page Screenshot
    _Click And Wait Element    //span[.="${category}"]
    Comment    _Click And Wait Element    (//span[.="เพิ่มสินค้า"])[1]
    Comment    Sleep    5s
    Comment    _Click And Wait Element    (//div[@class="cart"])[1]
    Comment    _Click And Wait Element    //button[.="ดำเนินการสั่งซื้อ"]
    Comment    Capture Page Screenshot

Verify Product in Mini Cart
    Click Element    xpath=(//div[@class="badge"])[1]
    Wait Until Page Contains    คิเรอิ คิเรอิ โฟมล้างมือ 200 มล.

Check Page Items
    [Arguments]    ${TotalPerPage}
    FOR    ${i}    IN RANGE    1    ${TotalPerPage}
        Log    ${i}
    END

Open URL
    [Arguments]    ${URL}
    _Open And Maximize Browser    https://www.bigc.co.th/${URL}    gc

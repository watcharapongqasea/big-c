*** Settings ***
Library           requests
Library           SeleniumLibrary

*** Test Cases ***
TC_Test
    # Get data from API    >>> Requests
    # Get max data
    # Loop Check items on web page
    Wait Until Element Is Visible    (//div[.="แสดง :"]/..//select[@class="custom-select"])[1]
    Select From List By Label    (//div[.="แสดง :"]/..//select[@class="custom-select"])[1]    100
    ${GetItemsPerPage}    SeleniumLibrary.Get Element Count    //div[@class="product-list-item col-md-3"]
    Should Be Equal    ${GetItemsPerPage}    100
    # 1000 rows

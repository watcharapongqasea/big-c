*** Settings ***
Test Teardown     Close All Browsers
Resource          ../Resource/Keywords/common.robot
Library           ExcelLibrary
Library           String
Resource          ../Resource/Keywords/KeywordsProduct.robot

*** Test Cases ***
TC_Test_Excel
    Open Excel Document    ${CURDIR}//test_noom.xlsx    test
    ${data}    Read Excel Row    row_num=2
    Close All Excel Documents
    Log    URL : ${data}[0]
    Log    SKU : ${data}[1]
    ${URL}    Set Variable    ${data}[0]
    ${SKU}    Fetch From Right    ${URL}    0-ml
    ${SKU}    Fetch From Left    ${SKU}    .html
    Should Be Equal    '${data}[1]'    '${SKU}'
    Log    ${URL}
    Open URL    ${URL}

*** Settings ***
Test Teardown     Close All Browsers
Resource          ../Resource/Keywords/common.robot
Library           ExcelLibrary
Library           String
Resource          ../Resource/Keywords/KeywordsProduct.robot

*** Test Cases ***
TC_01_Add2CartByCovidProduct
    Open BigC Website
    Select Product Hightlight    พร้อมรับมือโควิท-19

TC_02_Add2CartBySearch
    Open BigC Website
    Search Product    ผัก

TC_Nut_Click Login
    _Open And Maximize Browser    https://www.bigc.co.th/    gc
    Sleep    10s
    Run Keyword And Ignore Error    _Click And Wait Element    (//div[@class="om-global-close-button om-popup-close"])[1]
    Click Element    xpath=(//*[text()="เข้าสู่ระบบ"])[1]

TC_Ked_VerifyProduct
    Open BigC Website
    Select Product Hightlight    พร้อมรับมือโควิท-19
    Verify Product in Mini Cart

TC_noom2_enteremail
    Wait Until Element Is Visible    xpath=(//input[@placeholder="อีเมล"]) "owancy99@hotmail.com"
    Click Element    xpath=(//input[@placeholder="อีเมล"]) "owancy99@hotmail.com"

TC_Test
    Comment    Open BigC Website
    Comment    Select Product Hightlight    สินค้าส่งด่วน 1 ชม.
    Comment    Sleep    3s
    Comment    Capture Page Screenshot
    Comment    Wait Until Element Is Visible    (//div[.="แสดง :"]/..//select[@class="custom-select"])[1]    30s
    Comment    Select From List By Label    (//div[.="แสดง :"]/..//select[@class="custom-select"])[1]    100
    Comment    Wait Until Element Is Visible    //div[@class="product-list-item col-md-3"]    100s
    Comment    ${TotalPerPage}    SeleniumLibrary.Get Element Count    //div[@class="product-list-item col-md-3"]
    Comment    Should Be Equal    '${GetItemsPerPage}'    '100'
    Comment    Sleep    3s
    Comment    Capture Page Screenshot
    Comment    _Click And Wait Element    //span[@aria-label="Go to last page"]
    Comment    ${LastPage}    Get Text    //li[@class="page-item active"]
    ${LastPage}    Set Variable    10
    ${LastPage}    Evaluate    ${LastPage} + 1
    FOR    ${i}    IN RANGE    1    ${LastPage}
        Log Many    Page : ${i}
        Check Page Items    10
    END

TC_Test_Excel
    Open Excel Document    ${CURDIR}//test.xlsx    test
    ${data}    Read Excel Row    row_num=2
    Close All Excel Documents
    Log    URL : ${data}[0]
    Log    SKU : ${data}[1]
    ${URL}    Set Variable    ${data}[0]
    ${SKU}    Fetch From Right    ${data}[0]    ea-
    ${SKU}    Fetch From Left    ${SKU}    .html
    Should Be Equal    '${data}[1]'    '${SKU}'
    Open URL    ${URL}
    Log    sss

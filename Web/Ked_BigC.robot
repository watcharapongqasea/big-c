*** Settings ***
Library           SeleniumLibrary
Resource          ../Resource/Keywords/common.robot

*** Test Cases ***
TC_01_Register to BigC online
    Open Browser    https://www.bigc.co.th/    gc
    Wait Until Element Is Visible    xpath=//a[@href="/register"]    60s
    Click Element    xpath=//a[@href="/register"]
    Wait Until Element Is Visible    xpath=//input[@id="input-1"]    60s
    Input Text    id=input-1    fround1973@armyspy.com
    Input Text    id=telephone_number-1    0233456798
    Input Password    id=input-password-1    Aa123456
    Input Password    id=password_confirm-1    Aa123456
    Click Button    xpath=//button[text()="ต่อไป"]
    Wait Until Element Is Visible    xpath=//input[@id="firstname"]    60s
    Input Text    xpath=//input[@id="firstname"]    ทดสอบ
    Input Text    xpath=//input[@id="lastname"]    โรบอท
    Click Element    xpath=//*[text()="หญิง"]
    Select From List By Label    xpath=//*[@class="date-dropdown-select year custom-select"]    2535
    Select From List By Label    xpath=//*[@class="date-dropdown-select month custom-select"]    ธันวาคม
    Select From List By Label    xpath=//*[@class="date-dropdown-select day custom-select"]    31
    Click Element    xpath=//label[@for="checkbox-1"]
    Click Button    xpath=(//button[text()="สมัครสมาชิก"])[1]
    Wait Until Element Is Visible    xpath=(//button[text()="อัพเดทภายหลัง"])[2]    60s
    Click Button    xpath=(//button[text()="อัพเดทภายหลัง"])[2]

TC_02_Login to BigC Online
    _Open And Maximize Browser    https://www.bigc.co.th/    gc
    _Click And Wait Element    xpath=(//*[text()="เข้าสู่ระบบ"])[1]
    Run Keyword And Continue On Failure    Wait Until Element Is Visible    xpath=//*[@class="om-outer-canvas"]    80s
    Run Keyword And Continue On Failure    _Click And Wait Element    xpath=(//*[@class="om-popup-close-x"])[1]
    Run Keyword And Continue On Failure    _Click And Wait Element    xpath=//div[@aria-label="ปิด"]
    Wait Until Element Is Visible    id=login-modal___BV_modal_content_    60s
    _Input And Wait Element    id=email    fround1973@armyspy.com
    _Input And Wait Element    id=passwordLogin    Aa123456
    _Click And Wait Element    id=btn-login
    Wait Until Page Contains    คุณทดสอบ    60s

TC_03_Add_To_Cart
    Wait Until Page Contains    คุณทดสอบ    60s

TC_04_Add_To_Cart_And_Check_Total_Price
    Wait Until Page Contains    คุณทดสอบ    60s

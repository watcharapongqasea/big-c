*** Settings ***
Test Teardown     Close All Applications
Library           AppiumLibrary

*** Test Cases ***
TEST_CONs_Devices_Android
    Open Application    http://localhost:4723/wd/hub    platformName=Android    platformVersion=10    deviceName=K5J0220402007492    app=C:\\BigC\\com.t2p.bigc-2.0.6-free-www.apksum.com.apk    appPackage=com.t2p.bigc    appActivity=com.t2p.bigc.Launcher
    Comment    Open Application    http://localhost:4723/wd/hub    platformName=Android    platformVersion=10    deviceName=K5J0220402007492    appackage=com.t2p.bigc    appActivity=com.t2p.bigc.Launcher
    Wait Until Element Is Visible    //*[@resource-id="com.t2p.bigc:id/btn_skip"]    30s
    Click Element    //*[@resource-id="com.t2p.bigc:id/btn_skip"]
    Wait Until Element Is Visible    //*[@resource-id="com.t2p.bigc:id/menuSettings"]    30s
    Click Element    //*[@resource-id="com.t2p.bigc:id/menuSettings"]

TEST_CONs_Emulator_Android
    Open Application    http://localhost:4723/wd/hub    platformName=Android    platformVersion=11    deviceName=emulator-5554    app=C:\\Git\\smart-pension-engine-with-robot-framework\\Mobile\\mm.apk    appackage=com.calea.echo    appActivity=com.calea.echo.MainActivity
    Wait Until Element Is Visible    //*[@content-desc="More options"]
    AppiumLibrary.Click Element    //*[@content-desc="More options"]
    AppiumLibrary.Capture Page Screenshot
    Wait Until Element Is Visible    id=com.calea.echo:id/title
    AppiumLibrary.Click Element    id=com.calea.echo:id/title
    Sleep    3s
    AppiumLibrary.Capture Page Screenshot
    AppiumLibrary.Close Application

TEST_CONs_Devices_Android_BigC
    Open Application    http://localhost:4723/wd/hub    platformName=Android    platformVersion=11    deviceName=emulator-5554    app=C:\\Users\\Watcharapong\\Desktop\\bigc-uat-release-2.3.0(705)\\bigc-uat-release-2.3.0.apk    appPackage=com.t2p.bigc.uat    appActivity=com.t2p.bigc.Launcher
    Wait Until Element Is Visible    //*[@resource-id="${Env}:id/btn_skip"]    30s
    Click Element    //*[@resource-id="${Env}:id/btn_skip"]
    Wait Until Element Is Visible    //*[@resource-id="com.t2p.bigc.uat:id/menuShoppingOnline"]    30s
    Click Element    //*[@resource-id="com.t2p.bigc.uat:id/menuShoppingOnline"]
    Sleep    3s
    Capture Page Screenshot
    Run Keyword And Ignore Error    Wait Until Element Is Visible    //*[@text="While using the app"]    30s
    Run Keyword And Ignore Error    Click Element    //*[@text="While using the app"]
    Run Keyword And Ignore Error    Wait Until Element Is Visible    //*[@resource-id="com.t2p.bigc.uat:id/tvClose"]    30s
    Run Keyword And Ignore Error    Click Element    //*[@resource-id="com.t2p.bigc.uat:id/tvClose"]
    Sleep    10s
    ${Source}    Get Source
    Log    ${Source}
    Sleep    3s
    Capture Page Screenshot
    Sleep    5s
    Wait Until Element Is Visible    //*[@text="อาหารสด, แช่แข็ง/ ผักผลไม้"]/../..//*[@resource-id="com.t2p.bigc.uat:id/iv_category"]    30s
    Click Element    //*[@text="อาหารสด, แช่แข็ง/ ผักผลไม้"]/../..//*[@resource-id="com.t2p.bigc.uat:id/iv_category"]
    ${Source}    Get Source
    Log    ${Source}
    Sleep    3s
    Capture Page Screenshot
    Wait Until Element Is Visible    //android.widget.Button[@resource-id="com.t2p.bigc.uat:id/bt_add_product"]    30s
    Click Element    //android.widget.Button[@resource-id="com.t2p.bigc.uat:id/bt_add_product"]
    Sleep    15s
    Capture Page Screenshot
